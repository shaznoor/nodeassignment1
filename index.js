const http = require('http');
const fs = require('fs');

http
  .createServer((request, response) => {
    if (request.method === 'GET') {
      response.writeHead(200, { 'Content-Type': 'application/json' });

      fs.readFile('./text.json', 'utf-8', (err, options) => {
        if (err) {
          console.log(err);
        } else {
          try {
            const data = JSON.parse(options);
            fs.writeFile('./text.txt', JSON.stringify(data, null, 2), (err) => {
              if (err) {
                console.log(err);
              }
            });
          } catch (err) {
            console.log('Error', err);
          }
        }
      });
      response.end();
    }
  })
  .listen(8080);
